what does it do?
certiport_setup-server.ps1 helps automate the install of the certiport software
and to configure the local user account.

skills used:
> powershell

requirements:
> powershell

how to run:
1. run certiport_setup-server.ps1
2. configure certiport application
