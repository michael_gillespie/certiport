# get password from server
$pw = Get-Content \\w-sch-staff-13.hcps.internal\Share\CTE\cte_info.txt
$password = ConvertTo-SecureString $pw -AsPlainText -Force

# create user account
New-LocalUser -Name "certiportadmin" -Password $password -AccountNeverExpires -PasswordNeverExpires -UserMayNotChangePassword -Description "CTE testing account"

# add user to Administrators group
Add-LocalGroupMember -Group "Administrators" -Member "certiportadmin"

# install Console_Setup.exe
explorer.exe \\w-sch-staff-13.hcps.internal\Share\CTE\
